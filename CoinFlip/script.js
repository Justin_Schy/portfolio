"use strict";

const totalFlips = document.getElementById("totalText");
const headsText = document.getElementById("hText");
const tailsText = document.getElementById("tText");
const mainCoin = document.querySelector(".coinMain");

let result = 0;
let click = 0;
let heads = 0;
let tails = 0;

mainCoin.onclick = () => flip();

function flip() {
	click++;
	result = parseInt(Math.random() * 10);
	if (result % 2 == 0) {
		heads++;
	} else {
		tails++;
	}
	result = result * 180;

	flipCoin();
}

function flipCoin() {
	if (click % 2 == 0) {
		mainCoin.style.transform = `rotateY(${result}deg)`;
	} else {
		mainCoin.style.transform = `rotateY(-${result}deg)`;
	}
	setTimeout(function () {
		updateFlips();
	}, 650);
}

function updateFlips() {
	totalFlips.innerText = `Total Flips: ${heads + tails}`;
	headsText.innerText = `Heads: ${heads}`;
	tailsText.innerText = `Tails: ${tails}`;
}
