"use strict";

const navP1 = document.getElementById("p1");
const navP2 = document.getElementById("p2");

const infoPage = document.getElementById("info");
const gradePage = document.getElementById("grade");

const nameIndicator = document.getElementById("nameInd");
const namePlate = document.getElementById("namePlate");

const nameInput = document.getElementById("name");
const earnedInput = document.getElementById("earned");
const totalInput = document.getElementById("possible");
//Needed to use let because this is added dynamically and found no other way to make it work
let newStudentName = document.getElementById("newStudentName");

const submitButton = document.getElementById("submit");
const newStudentButton = document.getElementById("newStudentSubmit");

const gradeTable = document.getElementById("gradeTable");
const tableBody = document.getElementById("body");
const studentsTable = document.getElementById("studentsTable");
const studentBody = document.getElementById("studentBody");

const mainImg = document.getElementById("image");
const bottomText = document.querySelector("#bottomText");

let myRecords = [["Justin Schy"], ["Andy Pape"]];
let currentUser = 0;

/**
 * CHANGING PAGE
 */
navP1.addEventListener("click", function () {
	changePage("page1");
});
navP2.addEventListener("click", function () {
	changePage("page2");
});

/**
 * Changes the page to the page you chose
 * Page 1 or Page 2
 * @param {*} clickedPage
 */
function changePage(clickedPage) {
	if (clickedPage === "page1") {
		infoPage.classList.remove("notActive");
		gradePage.classList.add("notActive");
		changeImage();
	} else {
		infoPage.classList.add("notActive");
		gradePage.classList.remove("notActive");
		populateTable();
	}
}

/**
 * Getting and Adding Data
 */
nameInput.addEventListener("keyup", checkValues);
earnedInput.addEventListener("keyup", checkValues);
totalInput.addEventListener("keyup", checkValues);
submitButton.onclick = () => submitInfo();

/**
 * Checks the values of the three text boxes and disabeles the button
 */
function checkValues() {
	if (nameInput.value && earnedInput.value && totalInput.value) {
		submitButton.disabled = false;
	}
}

/**
 * changes the button every time you load the page
 */
function changeImage() {
	let img = Math.floor(Math.random() * 3) + 1;
	mainImg.src = `Images/Gifs/${img}.gif`;
}

/**
 * submits the information and appaneds it to the table
 */
function submitInfo() {
	//Appending the new record to the current user
	if (nameInput.value && earnedInput.value && totalInput.value) {
		myRecords[currentUser].push({
			name: nameInput.value,
			points: earnedInput.value,
			total: totalInput.value,
			percent: calcGrade(earnedInput.value, totalInput.value),
			button: `<button onClick='deleteRow(this)'><i class="fas fa-eraser"></i></button>`
		});

		updateStorage();
		populateTable();
		submitButton.disabled = true;
		clearInputs();
		calcCurrentGrade();
	} else {
		alert("Missing Info");
	}
}

/**
 * clears the input boxes
 */
function clearInputs() {
	nameInput.value = "";
	earnedInput.value = "";
	totalInput.value = "";
}

/**
 * calculates the current grade
 * @param {*} earned The grade recievd
 * @param {*} possible  the total number of points
 */
function calcGrade(earned, possible) {
	let grade = (earned / possible) * 100;
	grade = grade.toFixed(2);
	let html = "";

	if (grade >= 90) {
		html = `A - (${grade}%)`;
	} else if (grade >= 80) {
		html = `B - (${grade}%)`;
	} else if (grade >= 70) {
		html = `C - (${grade}%)`;
	} else if (grade >= 60) {
		html = `D - (${grade}%)`;
	} else {
		html = `F - (${grade}%)`;
	}
	return html;
}

/**
 * removes a row
 * @param {} roww
 */
function deleteRow(row) {
	if (confirm("Are you sure you want to delete this row?")) {
		let dRow = row.parentNode.parentNode.rowIndex;
		myRecords[currentUser].splice(dRow, 1);
		updateStorage();
		populateTable();
		calcCurrentGrade();
	}
}

/**
 * Switches the current user to a different user
 * @param {*} name
 */
function switchUser(name) {
	myRecords.forEach((item, index) => {
		if (item.includes(name)) {
			currentUser = index;
		}
	});

	//changes texts
	nameIndicator.innerText = `For: ${name}`;
	namePlate.innerText = `${name}'s Gradebook`;
	populateTable();
}

/**
 * Clears the whole table except the header
 */
function clearTable() {
	tableBody.innerHTML = "";
}

/**
 * Clears Students Table
 */
function clearStudentTable() {
	studentBody.innerHTML = "";
	let row = studentBody.insertRow(-1);

	let textBox = row.insertCell(0);
	let buttonBox = row.insertCell(1);

	textBox.innerHTML = `<input type="text" placeholder="Student Name" id="newStudentName" />`;
	buttonBox.innerHTML = `<button id="newStudentSubmit" onclick="createStudent()">Create New Student</button>`;
	newStudentName = document.getElementById("newStudentName");
	updateStorage();
}

/**
 * Updates the Current Storage
 */
function updateStorage() {
	localStorage.setItem("myRecords", JSON.stringify(myRecords));
}

/**
 * gets the current storage then puts it in myRecords
 */
function getStorage() {
	myRecords = JSON.parse(localStorage.getItem("myRecords"));
	if (!myRecords) {
		myRecords = [["Justin Schy"], ["Andy Pape"]];
		updateStorage();
		getStorage();
	}
}

/**
 * Uses the Records Array to populate the table with
 * the records in the array for current user
 */
function populateTable() {
	clearTable();
	getStorage();
	myRecords[currentUser].forEach((item, index) => {
		if (index != 0) {
			let row = tableBody.insertRow(-1);
			row.classList.add("tableRow");

			let nameCell = row.insertCell(0);
			let pointsCell = row.insertCell(1);
			let totalCell = row.insertCell(2);
			let percentCell = row.insertCell(3);
			let buttonCell = row.insertCell(4);

			nameCell.innerHTML = item.name;
			pointsCell.innerHTML = item.points;
			totalCell.innerHTML = item.total;
			percentCell.innerHTML = item.percent;
			buttonCell.innerHTML = item.button;
		}
	});
	calcCurrentGrade();
}

/**
 * populates students from current storage
 */
function populateStudents() {
	getStorage();
	clearStudentTable();
	myRecords.forEach((item, index) => {
		let row = studentBody.insertRow(index);

		let nameCell = row.insertCell(0);
		let button = row.insertCell(1);

		nameCell.innerHTML = item[0];
		button.innerHTML = `<button onClick="switchUser('${item[0]}')">Switch</button>`;
	});
}

/**
 * Adds a new Student to the Records Array
 */
function createStudent() {
	if (newStudentName.value) {
		if (confirm(`Creating User with name ${newStudentName.value}`)) {
			myRecords.push([`${newStudentName.value}`]);
			newStudentName.value = "";
			updateStorage();
			populateStudents();
		}
	} else {
		alert("No Student Name Entered");
	}
}

/**
 * Calculates the current overall grade
 */
function calcCurrentGrade() {
	let total = 0;
	let earned = 0;
	let text = "";
	myRecords[currentUser].map((item) => {
		if (typeof item === "object") {
			total += Number(item.total);
			earned += Number(item.points);

			text = calcGrade(earned, total);
		}
		if (myRecords[currentUser].length === 1) {
			text = "No Grades Entered";
		}
	});

	//changes texts
	bottomText.innerText = `Current Grade: ${text}`;
}
