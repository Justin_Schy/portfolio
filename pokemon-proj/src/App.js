import "./App.css";
import { useState } from "react";
import List from "./components/List";
import PokemonView from "./components/PokemonView";
import PokemonAdd from "./components/PokemonAdd";

function App() {
	const [currentComponent, setCurrentComponent] = useState("list");
	const [currentId, setCurrentId] = useState("");

	return (
		<div>
			{currentComponent === "list" && <List setComponent={setCurrentComponent} setId={setCurrentId} />}
			{currentComponent === "view" && <PokemonView id={currentId} setComponent={setCurrentComponent} />}
			{currentComponent === "add" && <PokemonAdd setComponent={setCurrentComponent} />}
		</div>
	);
}

export default App;
