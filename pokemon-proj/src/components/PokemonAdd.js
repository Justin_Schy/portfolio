import Axios from "axios";
import { makeStyles } from "@material-ui/core";
import { useState } from "react";
import TextField from "@material-ui/core/TextField";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ShuffleIcon from "@material-ui/icons/ShuffleOutlined";
import IconButton from "@material-ui/core/IconButton";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import Button from "@material-ui/core/Button";

const pokemonUrl = `https://pokeapi.co/api/v2/pokemon`;
const mainUrl = "https://web-app-pokemon.herokuapp.com";
const UserId = "I-Am-Inevitable";

const useStyles = makeStyles({
	dataBox: {
		display: "block",
		margin: "25px  0px",
		marginLeft: "25px"
	},
	mainBox: {
		display: "block",
		margin: "0px auto 0px",
		width: "60%"
	},
	nameBox: {
		margin: "25px  0px",
		marginLeft: "25px"
	},
	icon: {
		marginTop: "22px",
		marginLeft: "10px"
	}
});

function PokemonAdd(props) {
	const [Name, setName] = useState("");
	const [Type, setType] = useState("");
	const [Desc, setDesc] = useState("");
	const [Img, setImg] = useState("");
	const [OpenSb, setOpenSb] = useState(false);
	const [message, setMessage] = useState("");
	const [severity, setSev] = useState("success");

	const classes = useStyles();

	async function generateRandomPokemon() {
		try {
			const response = await Axios.get(`${pokemonUrl}/${Math.floor(Math.random() * 200) + 1}`);
			setName(response.data.forms[0].name);
			setType(response.data.types[0].type.name);
			setImg(response.data.sprites.front_default);
			let height = Math.round(response.data.height * 3.93701);
			let weight = Math.round(response.data.weight * 0.220462);
			setDesc(
				`This Pokemon standing at ${height} inches and weighing in at ${weight} lbs, has the ability ${response.data.abilities[0].ability.name} as well as the move ${response.data.moves[0].move.name}.`
			);
			setMessage("Successfully Received Random Pokemon");
			setSev("success");
			setOpenSb(true);
		} catch (e) {
			setMessage(`${e}`);
			setSev("error");
			setOpenSb(true);
		}
	}

	function add() {
		if (Name && Type && Img && Desc) {
			postPokemon();
			setMessage("Created New Pokemon");
			setSev("success");
			setOpenSb(true);
			props.setComponent("list");
		} else {
			setMessage("Missing Data from Fields");
			setSev("warning");
			setOpenSb(true);
		}
	}

	async function postPokemon() {
		try {
			await Axios({
				method: "post",
				url: `${mainUrl}/pokemon`,
				headers: {
					"User-Id": UserId,
					"Content-Type": "application/json"
				},
				data: {
					name: Name,
					type: Type,
					description: Desc,
					image: Img
				}
			});
		} catch (e) {
			setMessage(`${e}`);
			setSev("error");
			setOpenSb(true);
		}
	}

	function Alert(props) {
		return <MuiAlert elevation={6} variant="filled" {...props} />;
	}

	const handleClose = (event, reason) => {
		if (reason === "clickaway") {
			return;
		}

		setOpenSb(false);
	};

	return (
		<div className={classes.mainBox}>
			<form noValidate autoComplete="off">
				<TextField
					id="NameBox"
					label="Name"
					variant="outlined"
					className={classes.nameBox}
					onChange={(event) => {
						setName(event.target.value);
					}}
					value={Name}
				/>
				<IconButton
					aria-label="Back"
					className={classes.icon}
					onClick={(event) => {
						generateRandomPokemon();
					}}
				>
					<ShuffleIcon fontSize="large" color="primary" />
				</IconButton>
			</form>
			<form noValidate autoComplete="off">
				<TextField
					id="TypeBox"
					label="Type"
					variant="outlined"
					className={classes.dataBox}
					onChange={(event) => {
						setType(event.target.value);
					}}
					value={Type}
				/>
			</form>
			<form noValidate autoComplete="off">
				<TextField
					id="UrlBox"
					label="Image"
					fullWidth
					variant="outlined"
					className={classes.dataBox}
					onChange={(event) => {
						setImg(event.target.value);
					}}
					helperText="Must be a url"
					value={Img}
				/>
			</form>
			<form noValidate autoComplete="off">
				<TextField
					id="DescBox"
					label="Description"
					fullWidth
					multiline
					rows={4}
					variant="outlined"
					className={classes.dataBox}
					onChange={(event) => {
						setDesc(event.target.value);
					}}
					value={Desc}
				/>
			</form>
			<IconButton
				aria-label="Back"
				onClick={() => {
					props.setComponent("list");
				}}
			>
				<ArrowBackIcon />
			</IconButton>

			<Button variant="outlined" color="primary" onClick={add}>
				Add Pokemon
			</Button>
			<Snackbar open={OpenSb} autoHideDuration={6000} onClose={handleClose} severity={severity}>
				<Alert onClose={handleClose} severity={severity}>
					{message}
				</Alert>
			</Snackbar>
		</div>
	);
}

export default PokemonAdd;
