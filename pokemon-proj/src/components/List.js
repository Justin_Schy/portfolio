import { useState, useEffect } from "react";
import axios from "axios";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles, withStyles } from "@material-ui/core";

const PokemonUrl = "https://web-app-pokemon.herokuapp.com";
const UserId = "I-Am-Inevitable";

const useStyles = makeStyles({
	table: {
		width: "90%",
		margin: "10px auto 0px"
	},
	floatButton: {
		display: "block",
		margin: "30px 70px 0px auto"
	},
	image: {
		display: "block",
		width: "40%",
		marginRight: "auto",
		marginLeft: "auto"
	}
});

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: theme.palette.primary.main,
		color: theme.palette.secondary.main,
		fontSize: 25
	},
	body: {
		fontSize: 19
	}
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		"&:nth-of-type(odd)": {
			backgroundColor: theme.palette.action.hover
		}
	}
}))(TableRow);

function List(props) {
	const [pokemon, setPokemon] = useState([]);
	const [update, setUpdate] = useState(0);
	const classes = useStyles();

	useEffect(() => {
		async function getPokemon() {
			try {
				const response = await axios({
					method: "get",
					url: `${PokemonUrl}/pokemon`,
					headers: {
						"User-Id": UserId
					}
				});
				setPokemon(response.data);
			} catch (e) {
				throw e;
			}
		}
		getPokemon();
	}, [update]);

	async function deletePokemon(id, name) {
		if (window.confirm(`are you sure you want to delete ${name}`)) {
			try {
				await axios({
					method: "delete",
					url: `${PokemonUrl}/pokemon/${id}`,
					headers: {
						"User-Id": UserId
					}
				});
				setUpdate(update + 1);
			} catch (e) {
				throw e;
			}
		}
	}

	return (
		<div>
			<img
				src={
					"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsQ3Jqj48s7vj_XpvFu5cb56UG1Kky8ZnDDQ&usqp=CAU"
				}
				alt={"Pokemon Logo"}
				className={classes.image}
			></img>
			<TableContainer>
				<Table className={classes.table}>
					<TableHead>
						<StyledTableRow>
							<StyledTableCell align="left">Name</StyledTableCell>
							<StyledTableCell align="left">Type</StyledTableCell>
							<StyledTableCell align="right"></StyledTableCell>
						</StyledTableRow>
					</TableHead>
					<TableBody>
						{pokemon.map((item) => (
							<StyledTableRow key={item.id}>
								<StyledTableCell>{item.name}</StyledTableCell>
								<StyledTableCell>{item.type}</StyledTableCell>
								<StyledTableCell align="right">
									<IconButton
										aria-label="view"
										onClick={() => {
											props.setId(item.id);
											props.setComponent("view");
										}}
									>
										<VisibilityIcon />
									</IconButton>
									<IconButton
										aria-label="delete"
										onClick={() => {
											deletePokemon(item.id, item.name);
										}}
									>
										<DeleteIcon />
									</IconButton>
								</StyledTableCell>
							</StyledTableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
			<div>
				<Fab
					color="primary"
					aria-label="add"
					className={classes.floatButton}
					onClick={() => {
						props.setComponent("add");
					}}
				>
					<AddIcon />
				</Fab>
			</div>
		</div>
	);
}

export default List;
