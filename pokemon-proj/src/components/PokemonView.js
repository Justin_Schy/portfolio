import axios from "axios";
import { makeStyles } from "@material-ui/core";
import { useEffect, useState } from "react";
import background from "../images/electric.png";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import IconButton from "@material-ui/core/IconButton";

const UserId = "I-Am-Inevitable";
const PokemonUrl = "https://web-app-pokemon.herokuapp.com";

const useStyles = makeStyles({
	container: {
		width: "27%",
		margin: "100px auto 0px",
		border: "2px solid black",
		boxShadow: "4px 7px 3px rgba(0,0,0,.3)",
		backgroundColor: "rgba(255,225,102,255)",
		borderRadius: "15px",
		fontFamily: "pokemon"
	},
	innerCard: {
		display: "block",
		margin: "10px",
		backgroundImage: `url(${background})`,
		backgroundPosition: "center",
		backgroundRepeat: "no-repeat",
		backgroundSize: "cover",
		border: "1px solid black",
		borderRadius: "5px"
	},
	topText: {
		display: "flex",
		justifyContent: "space-between"
	},
	name: {
		marginLeft: "10px",
		marginTop: "-8px",
		fontFamily: "pokemon",
		fontSize: "30px"
	},
	type: {
		marginRight: "10px",
		marginTop: "-8px",
		fontFamily: "pokemon",
		fontSize: "30px"
	},
	subText: {
		marginLeft: "10px",
		fontSize: "10px"
	},
	imageFun: {
		display: "block",
		margin: "0px auto 10px",
		padding: "10px",
		backgroundColor: "rgb(255,255,255)",
		width: "80%",
		border: "5px solid",
		borderImage: " linear-gradient(to bottom right, silver, aliceblue) 10",
		borderBottom: "15px solid silver",
		boxShadow: "4px 7px 3px rgba(0,0,0,.3)"
	},
	image: {
		display: "block",
		margin: "0px auto 0px",
		maxHeight: "275px"
	},
	desc: {
		fontSize: "large"
	},
	Button: {}
});

function PokemonView(props) {
	const [pokemonData, setPokemonData] = useState([]);
	const classes = useStyles();

	useEffect(() => {
		async function getPokemonData() {
			try {
				const response = await axios({
					method: "get",
					url: `${PokemonUrl}/pokemon/${props.id}`,
					headers: {
						"User-Id": UserId
					}
				});
				setPokemonData(response.data);
			} catch (e) {
				throw e;
			}
		}
		getPokemonData();
	}, [props.id]);

	return (
		<div>
			<div className={classes.container}>
				<div className={classes.innerCard}>
					<sub className={classes.subText}>Basic Pokémon</sub>
					<div className={classes.topText}>
						<h4 className={classes.name}>{pokemonData.name}</h4>
						<h4 className={classes.type}>{pokemonData.type}</h4>
					</div>
					<div className={classes.imageFun}>
						<img className={classes.image} src={pokemonData.image} alt={pokemonData.name} />
					</div>
					<sub className={classes.desc}>{pokemonData.description}</sub>
				</div>
			</div>

			<IconButton
				aria-label="Back"
				onClick={() => {
					props.setComponent("list");
				}}
			>
				<ArrowBackIcon />
			</IconButton>
		</div>
	);
}
export default PokemonView;
